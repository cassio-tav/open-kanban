# open-kanban

A simple open-source kanban board with a history and the possibility of periodic reports

## Warning

This is intended as an amateur's attempt to implement a simple but usable Kanban with support for periodic reports and a history. So, no promisses: I'm just learning ─ use it at your own risk.

Additionally, I'm not even well versed in Git and GitHub. So, it's likely that I'll have some trouble with issues, pull requests, merges, etc. (I'd acually like to know how _Continuous integration_ works.)

**Help will be welcome**, so, if you're feeling generous, please help out!

## How I'm doing it

My first commit is just [SvelteKit](https://kit.svelte.dev/)'s demo App, which is a _todo app_. Then I'll rework the Todo List component into a Kanban Board. The next step is to persist the data with [SQLite](https://www.sqlite.org/), and for that I'm thinking of using [Prisma](https://www.prisma.io/).

## What else?

I intend to deploy a demo, possibly at Vercel.
